What is this mod?
It basically just sets the weather to be what you want it to be, and freezes it there.

Why make it?
It was requested.

How do I install it?
First, install .NET Scripthook.
Then, copy the 'PermanentWeather.net.dll' file in the rar to the scripts folder in your GTA IV directory.

How to use it?
Press RCtrl + W to toggle permanent weather.
Press the ~ key to open the console, then type setweather, space, then the weather that you want to set it to (list of possible weathers below).
Change the 'default_weather' in the ini file (which generates after first loading of script) to change... the default weather the script uses.

Possible weathers:
ExtraSunny
Sunny
SunnyAndWindy
Cloudy
Raining
Drizzle
Foggy
ThunderStorm
ExtraSunny2
SunnyAndWindy2
 
Credits:
RiceBox for the request.
LetsPlayOrDy (now named Jitnaught) for the script.

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
