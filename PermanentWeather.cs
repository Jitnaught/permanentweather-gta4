﻿using GTA;
using System;
using System.IO;
using System.Windows.Forms;

namespace PermanentWeather
{
    public class PermanentWeather : Script
    {
        int weather;
        bool enabled = false;

        public PermanentWeather()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("default_weather","weather", "foggy");
                Settings.Save();
            }

            weather = WeatherToInt(Settings.GetValueString("default_weather", "weather", "foggy"));
            if (weather == -1)
            {
                Game.DisplayText("The default weather you set is unknown. The weather has been set to 'extrasunny'.");
                weather = 0;
            }

            BindConsoleCommand("setweather", consoleSetWeather, " - [string weather] Sets the permanent weather");

            Interval = 5000;
            Tick += PermanentWeather_Tick;

            KeyDown += PermanentWeather_KeyDown;
        }

        private void consoleSetWeather(ParameterCollection Parameter)
        {
            if (Parameter.Count == 1)
            {
                int tempWeather = WeatherToInt(Parameter[0]);
                if (tempWeather == -1)
                {
                    Game.DisplayText("That is an unknown weather. Please try again");
                    return;
                }
                weather = tempWeather;
                Game.DisplayText("Weather has been set");
            }
        }

        private void PermanentWeather_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if (isKeyPressed(Keys.RControlKey) && e.Key == Keys.W)
            {
                enabled = !enabled;
                Subtitle(enabled ? "PermanentWeather enabled" : "PermanentWeather disabled");
            }
        }

        private void PermanentWeather_Tick(object sender, EventArgs e)
        {
            if (enabled) SetWeather(weather);
        }

        private int WeatherToInt(string weather)
        {
            /*
            ExtraSunny = 0,
            Sunny = 1,
            SunnyAndWindy = 2,
            Cloudy = 3,
            Raining = 4,
            Drizzle = 5,
            Foggy = 6,
            ThunderStorm = 7,
            ExtraSunny2 = 8,
            SunnyAndWindy2 = 9
            */
            if (weather.Equals("extrasunny", StringComparison.OrdinalIgnoreCase)) return 0;
            else if (weather.Equals("sunny", StringComparison.OrdinalIgnoreCase)) return 1;
            else if (weather.Equals("sunnyandwindy", StringComparison.OrdinalIgnoreCase)) return 2;
            else if (weather.Equals("cloudy", StringComparison.OrdinalIgnoreCase)) return 3;
            else if (weather.Equals("raining", StringComparison.OrdinalIgnoreCase)) return 4;
            else if (weather.Equals("drizzle", StringComparison.OrdinalIgnoreCase)) return 5;
            else if (weather.Equals("foggy", StringComparison.OrdinalIgnoreCase)) return 6;
            else if (weather.Equals("thunderstorm", StringComparison.OrdinalIgnoreCase)) return 7;
            else if (weather.Equals("extrasunny2", StringComparison.OrdinalIgnoreCase)) return 8;
            else if (weather.Equals("sunnyandwindy2", StringComparison.OrdinalIgnoreCase)) return 9;
            return -1;
        }

        private void SetWeather(int weather)
        {
            if (World.Weather != (Weather)weather)
            {
                GTA.Native.Function.Call("FORCE_WEATHER_NOW", weather);
            }
        }

        private void Subtitle(string message, uint time = 1500)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", message, time, 1);
        }
    }
}
